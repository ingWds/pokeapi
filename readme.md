# Spring-oauth2-jpa-example
Basado en los servicios de https://pokeapi.co/ realizar un “sistema de administración personal de pokemons” con las siguientes funciones

Autenticación por usuario OAuth2. Proporcionar dos usuarios
Servicios para:
•	Obtener la lista de pokemons por usuario.
•	Insertar un pokemon a la lista del usuario.
•	Actualizar la lista completa de pokemons del usuario.
•	Actualizar las propiedades particulares de un pokemon.
•	Eliminar un pokemon de la lista del usuario
La entidad pokemon debe contar con las siguientes propiedades
Número de Pokedex Integer, No mutable
Nombre en Pokedex String, No mutable:
Imagen (URL) String, No mutable:
Alias String particular por usuario
Shiny Boolean particular por usuario
Nature String particular por usuario

Entregables:
Codigo en repositorio GIT 
Colección de Postman.
Script de base de datos

docker run -p 5432:5432 --name pokedb -e POSTGRES_USERNAME=postgres -e POSTGRES_PASSWORD=secret -d postgres
docker exec -it pokedbpsql -U postgres
CREATE USER pokedex PASSWORD 'secret';
ALTER ROLE pokedex WITH SUPERUSER;
CREATE DATABASE pokedb WITH OWNER pokedex;
GRANT ALL PRIVILEGES ON DATABASE pokedb TO pokedex;


INSERT INTO public.pokemon
(nom_pokedex, num_pokedex, url_imagen)
VALUES('charmander', 1, 'charmander.jpg');

INSERT INTO public.pokemon
(nom_pokedex, num_pokedex, url_imagen)
VALUES('squirtle', 2, 'squirtle.jpg');


INSERT INTO public.pokemon
(nom_pokedex, num_pokedex, url_imagen)
VALUES('Bulbasaur', 3, 'Bulbasaur.jpg');

INSERT INTO public.pokemon
(nom_pokedex, num_pokedex, url_imagen)
VALUES('Pikachu', 4, 'raichu.jpg');


INSERT INTO public.pokedex
(alias, maestro, nature, pokemon, shiny)
VALUES('chary', 'red', 'fuego', 1, 'a');

INSERT INTO public.pokedex
(alias, maestro, nature, pokemon, shiny)
VALUES('pika', 'ash', 'electrico', 4, 'b');
