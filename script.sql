
docker run -p 5432:5432 --name pokedb -e POSTGRES_USERNAME=postgres -e POSTGRES_PASSWORD=secret -d postgres
docker exec -it pokedbpsql -U postgres
CREATE USER pokedex PASSWORD 'secret';
ALTER ROLE pokedex WITH SUPERUSER;
CREATE DATABASE pokedb WITH OWNER pokedex;
GRANT ALL PRIVILEGES ON DATABASE pokedb TO pokedex;


INSERT INTO public.pokemon
(nom_pokedex, num_pokedex, url_imagen)
VALUES('charmander', 1, 'charmander.jpg');

INSERT INTO public.pokemon
(nom_pokedex, num_pokedex, url_imagen)
VALUES('squirtle', 2, 'squirtle.jpg');


INSERT INTO public.pokemon
(nom_pokedex, num_pokedex, url_imagen)
VALUES('Bulbasaur', 3, 'Bulbasaur.jpg');

INSERT INTO public.pokemon
(nom_pokedex, num_pokedex, url_imagen)
VALUES('Pikachu', 4, 'raichu.jpg');


INSERT INTO public.pokedex
(alias, maestro, nature, pokemon, shiny)
VALUES('chary', 'red', 'fuego', 1, 'a');

INSERT INTO public.pokedex
(alias, maestro, nature, pokemon, shiny)
VALUES('pika', 'ash', 'electrico', 4, 'b');