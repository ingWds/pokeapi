package com.gonet.nintendo.pokemon.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gonet.nintendo.pokemon.entities.Pokedex;
import com.gonet.nintendo.pokemon.repositories.PokedexRepository;
import com.gonet.nintendo.pokemon.repositories.PokemonRepository;


@RestController
@RequestMapping("/api/v1")
public class PokemonController {
	@Autowired
	private PokemonRepository pokemonRepository;
	@Autowired
	private PokedexRepository pokedexRepository;

//	@GetMapping("/pokemons")
//	public List<Pokemon> getAllPokemons() {
//		return pokemonRepository.findAll();
//	}
	///Obtener la lista de pokemons por usuario.
	@GetMapping("/entrenador/{nom_entrenador}")
	public List<Pokedex> getAllPokemonsEntrenador(@PathVariable("nom_entrenador") String nomEntrenador) {
		System.out.println("entrenador = "+nomEntrenador);
		List<Pokedex> resultado = null;
		try {
			resultado =  pokedexRepository.findByMaestro(nomEntrenador);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}	

	///Insertar un pokemon a la lista del usuario.
	@PostMapping("/entrenador/pokedex/{nom_entrenador}")
	public List<Pokedex> addPokemonEntrenador(@PathVariable("nom_entrenador") String nomEntrenador, @RequestBody Pokedex pokedex) {
		System.out.println("entrenador = "+nomEntrenador);
		List<Pokedex> resultado = null;
		try {
			pokedexRepository.save(pokedex);
			resultado =  pokedexRepository.findByMaestro(nomEntrenador);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}
	
	///Actualizar la lista completa de pokemons del usuario.
	@PostMapping("/entrenador/pokedexall/{nom_entrenador}")
	public List<Pokedex> addAllPokemonEntrenador(@PathVariable("nom_entrenador") String nomEntrenador, @RequestBody List<Pokedex> pokedex) {
		System.out.println("entrenador = "+nomEntrenador);
		List<Pokedex> resultado = null;
		try {
			for(Pokedex pokemon : pokedex) {
				pokedexRepository.save(pokemon);				
			}
			resultado =  pokedexRepository.findByMaestro(nomEntrenador);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}

	///Actualizar las propiedades particulares de un pokemon.
	@PostMapping("/entrenador/update/{nom_entrenador}")
	public List<Pokedex> updatePokemonEntrenador(@PathVariable("nom_entrenador") String nomEntrenador, @RequestBody Pokedex pokedex) {
		System.out.println("entrenador = "+nomEntrenador);
		List<Pokedex> resultado = null;
		try {
			pokedexRepository.save(pokedex);
			resultado =  pokedexRepository.findByMaestro(nomEntrenador);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}

	///Eliminar un pokemon de la lista del usuario
	@PostMapping("/entrenador/pokedex/remove/{nom_entrenador}/{id_pokemon}")
	public List<Pokedex> removePokemonEntrenador(@PathVariable("nom_entrenador") String nomEntrenador, @PathVariable("id_pokemon") long idPokemon) {
		System.out.println("entrenador = "+nomEntrenador);
		List<Pokedex> resultado = null;
		try {
			Pokedex pokedex = new Pokedex();
			pokedex.setId(idPokemon);
			pokedexRepository.delete(pokedex);
			resultado =  pokedexRepository.findByMaestro(nomEntrenador);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}
}
