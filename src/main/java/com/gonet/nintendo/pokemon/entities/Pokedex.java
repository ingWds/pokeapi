package com.gonet.nintendo.pokemon.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pokedex")
public class Pokedex{

	private long id;
    private String maestro;
    private long pokemon;
    private String alias;
    private String shiny;
    private String nature;
    
    public Pokedex(){
    	
    }
    
	Pokedex(long id, String maestro, long pokemon, String alias, String shiny, String nature) {
		this.id = id;
		this.maestro = maestro;
		this.pokemon = pokemon;
		this.alias = alias;
		this.shiny = shiny;
		this.nature = nature;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "maestro", nullable = false)
	public String getMaestro() {
		return maestro;
	}

	public void setMaestro(String maestro) {
		this.maestro = maestro;
	}

	@Column(name = "pokemon", nullable = false)
	public long getPokemon() {
		return pokemon;
	}


	public void setPokemon(long pokemon) {
		this.pokemon = pokemon;
	}

	@Column(name = "alias", nullable = false)
	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	@Column(name = "shiny", nullable = false)
	public String getShiny() {
		return shiny;
	}

	public void setShiny(String shiny) {
		this.shiny = shiny;
	}

	@Column(name = "nature", nullable = false)
	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}

	
}
