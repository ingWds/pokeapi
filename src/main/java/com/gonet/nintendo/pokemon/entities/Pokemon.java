package com.gonet.nintendo.pokemon.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pokemon")
public class Pokemon {

	private long id;
	private Integer numeroPokedex;
	private String nombrePokedex;
	private String urlImagen;
	
	public Pokemon() {
		
	}
	
	public Pokemon(Integer numeroPokedex, String nombrePokedex, String urlImagen) {
		this.numeroPokedex = numeroPokedex;
		this.nombrePokedex = nombrePokedex;
		this.urlImagen = urlImagen;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name = "num_pokedex", nullable = false)
	public Integer getNumeroPokedex() {
		return numeroPokedex;
	}

	public void setNumeroPokedex(Integer numeroPokedex) {
		this.numeroPokedex = numeroPokedex;
	}

	@Column(name = "nom_pokedex", nullable = false)
	public String getNombrePokedex() {
		return nombrePokedex;
	}

	public void setNombrePokedex(String nombrePokedex) {
		this.nombrePokedex = nombrePokedex;
	}

	@Column(name = "url_imagen", nullable = false)
	public String getUrlImagen() {
		return urlImagen;
	}

	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}

	@Override
	public String toString() {
		return "Pokemon [id=" + id + ", numeroPokedex=" + numeroPokedex + ", nombrePokedex=" + nombrePokedex + ", urlImagen=" + urlImagen
				+ "]";
	}
	
}
