package com.gonet.nintendo.pokemon.entities;

import javax.persistence.Entity;

@Entity
public class Role extends BaseEntity{

    private String name;

	public Role(String rol) {
		name = rol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    
}
