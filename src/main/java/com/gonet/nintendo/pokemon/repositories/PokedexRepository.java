package com.gonet.nintendo.pokemon.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gonet.nintendo.pokemon.entities.Pokedex;

@Repository
public interface PokedexRepository extends JpaRepository<Pokedex,Long> {
	List<Pokedex> findByMaestro(String maestro);
}
