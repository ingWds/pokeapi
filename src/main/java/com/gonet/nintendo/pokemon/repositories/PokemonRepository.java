package com.gonet.nintendo.pokemon.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gonet.nintendo.pokemon.entities.Pokemon;

@Repository
public interface PokemonRepository extends JpaRepository<Pokemon,Long> {
    
}
