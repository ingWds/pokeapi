package com.gonet.nintendo.pokemon.repositories;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

import com.gonet.nintendo.pokemon.entities.Usuario;

/**
 * Usuario repository for CRUD operations.
 */
public interface UserRepository extends JpaRepository<Usuario,Long> {
    Optional<Usuario> findByUsername(String username);
}
